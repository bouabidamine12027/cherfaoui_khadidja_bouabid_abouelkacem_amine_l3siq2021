# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 10:25:10 2021

@author: Crash
"""

# improter les bibliothèques nécessaires 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, confusion_matrix , classification_report
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve
from sklearn.preprocessing import StandardScaler
# phase1
# analyse exploitaire des données
data=pd.read_csv('Phase1.csv')
df=data.copy()
# les nombres de lignes x les nombres des colonnes
df.shape
# type de variable
df.dtypes.value_counts()
df.info()
# graphe Représente les types de variables utilisées
df.dtypes.value_counts().plot.pie()
for col in df.select_dtypes('int'):
    sns.distplot(df[col])
#  visualiser les variables quantitatifs
for col in df.select_dtypes('object'):
    print( col ,df[col].unique()) 
# graphe Représente  la variation des variables quantitative
for col in df.select_dtypes('object'):
    plt.figure()
    df[col].value_counts().plot.pie() 
#créer des tableau pour class anomale et normal
df_normal=df[df['class']=='normal']
df_anomale=df[df['class']=='anomaly']
for col in df.select_dtypes('float'):
    plt.figure()
    sns.distplot(df_normal[col], label='normal')
    sns.distplot(df_anomale[col], label='anomaly')
    plt.legend()
# analyse des valeurs manquantes
df.isna()
sns.heatmap(df.isna())
# selon le graphe pas des valeurs manquantes
df.isnull().sum()
# compter les classes normal et anomale
df['class'].value_counts()
# 98.8%  normal
# préparer les données
Label_encoder = LabelEncoder()
df ['class'] = Label_encoder.fit_transform (df ['class'])
df['service']= Label_encoder.fit_transform (df ['service']) 
df ['flag']= Label_encoder.fit_transform (df ['flag']) 
df ['protocol_type']= Label_encoder.fit_transform (df ['protocol_type'])
mise_a_echelle = StandardScaler()
df = mise_a_echelle.fit_transform(df)


y=df['class']
x=df.drop('class', axis=1) 
# on fait plusieurs test sur la valeur n neighbors
#pour n=5 on score1=0.9968 et pour n=4 on a 0.9975 et pour n=3 on a 0.9980392156862745
x_train, x_test, y_train, y_test=train_test_split(x,y, test_size=0.3)
model = KNeighborsClassifier(n_neighbors=3)
model.fit(x_train,y_train )
score_1=model.score(x_test,y_test)
def vp_vn(x_test,y_test):
    liste=[]
    y1=y_test
    y2=model.predict(x_test)
    vp=fp=vn=fn=0
    for i,j in y1,y2:
        if (i==1):
            
           if(j==1):
               vp+=1
           else:
               fp+=1
        else :
            if(j==0):
                  vn+=1
            else:
                fn+=1
    liste=[vp,fp,vn,fn]            
    return liste
# calcul vp vn fp fn

# utilisée la crosse validation

ypred=model.predict(x_test)
confusion_matrix(y_test,ypred)
print(classification_report(y_test,ypred))
# estimation si le model est bon
n, train_score, val_score = learning_curve(model, x_train, y_train, train_sizes=np.linspace(0.7, 1.0, 5), 
                                            cv=5, scoring='f1'  ) 
#  les résultats par un graphe 
plt.Figure(figsize=(12,8))
plt.plot(n, train_score.mean(axis=1), label='train_score' )
plt.plot(n, val_score.mean(axis=1), label='train_score' )
# le modele esi underfitted

