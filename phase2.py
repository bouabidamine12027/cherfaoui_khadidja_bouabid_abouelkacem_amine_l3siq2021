# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 19:24:10 2021

@author: Crash
"""
# improter les bibliothèques nécessaires 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, confusion_matrix , classification_report
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve
from sklearn.preprocessing import StandardScaler
#phase 2
# analyse exploitaire des données
data1=pd.read_csv('Phase2.csv')
df1=data1.copy()
df1.shape
# type de variable
df1.dtypes.value_counts()
df1.info()
# graphe Représente les types de variables utilisées
df1.dtypes.value_counts().plot.pie()
# d'apres le graphe de pie on le meme nombre de type de variale
for col in df1.select_dtypes('int'):
    sns.distplot(df1[col])
#  visualiser les variables quantitatifs
for col in df1.select_dtypes('object'):
    print( col ,df1[col].unique()) 
# graphe Représente  la variation des variables quantitative
for col in df1.select_dtypes('object'):
    plt.figure()
    df1[col].value_counts().plot.pie() 
#créer des tableau pour class anomale et normal
df_normal1=df1[df1['class']=='normal']
df_anomale1=df1[df1['class']=='anomaly']
for col in df1.select_dtypes('float'):
    plt.figure()
    sns.distplot(df_normal1[col], label='normal')
    sns.distplot(df_anomale1[col], label='anomaly')
    plt.legend()
# analyse des valeurs manquantes
df1.isna()
sns.heatmap(df1.isna())
# selon le graphe pas des valeurs manquantes
df1.isnull().sum()
# compter les classes normal et anomale
df1['class'].value_counts()
# 53.38%  normal et 46.62% anomale lechangement que les deux valeurs de class ont presque le meme 
# nombre 
# préparer les données
Label_encoder = LabelEncoder()
df1['class'] = Label_encoder.fit_transform (df1 ['class'])
df1['service']= Label_encoder.fit_transform (df1 ['service']) 
df1 ['flag']= Label_encoder.fit_transform (df1 ['flag']) 
df1 ['protocol_type']= Label_encoder.fit_transform (df1 ['protocol_type'])
# il ya des valeurs 0 et des valeurs entre 10359 donc on standarise les données
mise_a_echelle = StandardScaler()
df1= mise_a_echelle.fit_transform(df1)
y=df1['class']
x=df1.drop('class', axis=1) 
# on fait plusieurs test sur la valeur n neighbors
#pour n=5 on score1=0.9968 et pour n=4 on a 0.9975 et pour n=3 on a 0.9980392156862745
x_train, x_test, y_train, y_test=train_test_split(x,y, test_size=0.3)
model = KNeighborsClassifier(n_neighbors=3)
model.fit(x_train,y_train )
score_1=model.score(x_test,y_test)
# la valeur de score_1 est 0.9887
ypred=model.predict(x_test)
confusion_matrix(y_test,ypred)
print(classification_report(y_test,ypred))
# estimation si le model est bon
n, train_score, val_score = learning_curve(model, x_train, y_train, train_sizes=np.linspace(0.7, 1.0, 5), 
                                            cv=5, scoring='f1'  ) 
                                            cv=5, scoring='f1'  ) 
#  les résultats par un graphe 
plt.Figure(figsize=(12,8))
plt.plot(n, train_score.mean(axis=1), label='train_score' )
plt.plot(n, val_score.mean(axis=1), label='train_score' )
# le modele esi underfitted



