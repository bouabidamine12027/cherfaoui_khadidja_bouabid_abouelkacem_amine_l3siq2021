# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 20:31:13 2021

@author: Crash
"""
# improter les bibliothèques nécessaires 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_score 
#phase 3
# analyse exploitaire des données
data2=pd.read_csv('Phase3.csv')
df2=data2.copy()
df2.shape
# type de variable
df2.dtypes.value_counts()
df2.info()
# graphe Représente les types de variables utilisées
df2.dtypes.value_counts().plot.pie()
# d'apres le graphe de pie on le meme nombre de type de variale
for col in df2.select_dtypes('int'):
    sns.distplot(df2[col])
#  visualiser les variables quantitatifs
for col in df2.select_dtypes('object'):
    print( col ,df2[col].unique()) 
# graphe Représente  la variation des variables quantitative
for col in df2.select_dtypes('object'):
    plt.figure()
    df2[col].value_counts().plot.pie()
    # analyse des valeurs manquantes
df2.isna()
sns.heatmap(df2.isna())
# selon le graphe pas des valeurs manquantes
df2.isnull().sum()
#transformer les valeurs qualitatif en valeur quantitatif
Label_encoder = LabelEncoder()
df2['service']= Label_encoder.fit_transform (df2 ['service']) 
df2 ['flag']= Label_encoder.fit_transform (df2 ['flag']) 
df2 ['protocol_type']= Label_encoder.fit_transform (df2 ['protocol_type'])
mise_a_echelle = StandardScaler()
df_tr= mise_a_echelle.fit_transform(df2)
model = KMeans(n_clusters=2)
model.fit_transform( df_tr)
model.predict(df_tr)
labels=model.labels_
silhouette_score(df_tr, labels, metric='euclidean')
plt.scatter(df_tr[:,0],df_tr[:,1],c=model.predict(df_tr))

